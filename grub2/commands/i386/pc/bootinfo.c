/* pxe.c - command to control the pxe driver  */
/*
 *  GRUB  --  GRand Unified Bootloader
 *  Copyright (C) 2008  Free Software Foundation, Inc.
 *
 *  GRUB is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  GRUB is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with GRUB.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <grub/normal.h>
#include <grub/dl.h>
#include <grub/lib/arg.h>
#include <grub/err.h>
#include <grub/misc.h>
#include <grub/time.h>
#include <grub/term.h>
#include <grub/machine/pxe.h>
#include <grub/machine/memory.h>
#include <grub/mm.h>
#include <grub/device.h>
#include <grub/disk.h>
#include <grub/fs.h>
#include <grub/file.h>
#include <grub/partition.h>
#include <grub/msdos_partition.h>
#include <grub/bootwhat.h>
#include <grub/extcmd.h>

#define	USER_ABORT		-2
#define MAX_CMD_LENGTH		512 /* XXX Is this sane? */
#define MAX_CMD_ARGS		64

#define DHCP_SNAME_OFFSET	44
#define DHCP_FILE_OFFSET	108
#define DHCP_OPTIONS_OFFSET	236

#define DHCP_OPTION_PAD		0
#define DHCP_OPTION_VENDOR	43
#define DHCP_OPTION_OVERLOAD	52
#define DHCP_OPTION_VCI         60
#define DHCP_OPTION_END		255

#define EMULAB_VCI              "Emulab"

#define DHCP_OPTION_EMULAB_BOOTINFO     128

#define GET_KEY_SLEEP_INTERVAL	100

/* Stolen from fs/i386/pc/pxe.c */
#define SEGMENT(x)	((x) >> 4)
#define OFFSET(x)	((x) & 0xF)
#define SEGOFS(x)	((SEGMENT(x) << 16) + OFFSET(x))
#define LINEAR(x)	(void *) (((x >> 16) <<4) + (x & 0xFFFF))

int debug = 0;

/* typedef	grub_uint32_t	in_addr_t; */
#define htonl(x)	(grub_cpu_to_be32(x))
#define ntohl(x)	(grub_be_to_cpu32(x))
#define htons(x)	(grub_cpu_to_be16(x))

#if 0
static void print_packet(char *buffer, int length)
{
	int i;

	for (i = 0; i < length; i++) {
		if (i % 8 == 0) {
			grub_printf("\n");
		}
		grub_printf("%02x ", buffer[i]);
	}
}
#endif

/* get a key, timing out after at least timeout milliseconds */
static int get_key(int timeout)
{
	int key = -1;

	if (timeout < 0) {
		return grub_getkey();
	}

	while (timeout > 0) {
		key = grub_checkkey();

		if (key != -1) {
			key = grub_getkey();
			break;
		}

		grub_millisleep(GET_KEY_SLEEP_INTERVAL);
		timeout -= GET_KEY_SLEEP_INTERVAL;
	}

	return key;
}

static int pxe_get_cached_info(int type, char **buffer, grub_size_t *length)
{
	struct grub_pxenv_get_cached_info ci;

	char *data;

	ci.packet_type = type;
	ci.buffer = 0;
	ci.buffer_size = 0;
	grub_pxe_call (GRUB_PXENV_GET_CACHED_INFO, &ci);
	if (ci.status) {
		return ci.status;
	}

	data = grub_malloc(ci.buffer_size);
	if (data == NULL) {
		return GRUB_PXENV_STATUS_FAILURE;
	}

	memcpy(data, LINEAR(ci.buffer), ci.buffer_size);

	*length = ci.buffer_size;
	*buffer = data;

	return 0;
}

static int udp_open(in_addr_t src_ip)
{
	struct grub_pxenv_udp_open uo;

	uo.status = GRUB_PXENV_STATUS_FAILURE;
	uo.src_ip = src_ip;

	grub_pxe_call (GRUB_PXENV_UDP_OPEN, &uo);

	if (uo.status)
		return uo.status;

	return 0;
}

static int udp_close(void)
{
	struct grub_pxenv_udp_close uc;

	uc.status = GRUB_PXENV_STATUS_FAILURE;

	grub_pxe_call (GRUB_PXENV_UDP_CLOSE, &uc);

	if (uc.status)
		return uc.status;

	return 0;
}

static int udp_read(in_addr_t dst_ip, grub_uint16_t dst_port, void *buf, grub_uint16_t *size)
{
	struct grub_pxenv_udp_read ur;

	ur.buffer = SEGOFS (GRUB_MEMORY_MACHINE_SCRATCH_ADDR);
	ur.buffer_size = *size;
	ur.dst_ip = dst_ip;
	ur.dst_port = dst_port;

	ur.status = GRUB_PXENV_STATUS_FAILURE;
	grub_pxe_call (GRUB_PXENV_UDP_READ, &ur);

	if (ur.status)
		return ur.status;

	grub_memcpy((char *)buf, (char *)GRUB_MEMORY_MACHINE_SCRATCH_ADDR, ur.buffer_size);

	*size = ur.buffer_size;

	return 0;
}

static int udp_write(in_addr_t dest_ip, grub_uint16_t d_port, grub_uint16_t s_port,
              in_addr_t gateway, void *buf, grub_uint16_t length)
{
	struct grub_pxenv_udp_write uw;

	grub_memcpy((char *)GRUB_MEMORY_MACHINE_SCRATCH_ADDR, (char *)buf, length);

	uw.status = GRUB_PXENV_STATUS_FAILURE;
	uw.ip = dest_ip;
	uw.gateway = gateway;
	uw.src_port = s_port;
	uw.dst_port = d_port;
	uw.buffer_size = length;
	uw.buffer = SEGOFS (GRUB_MEMORY_MACHINE_SCRATCH_ADDR);

	grub_pxe_call (GRUB_PXENV_UDP_WRITE, &uw);

	if (uw.status)
		return uw.status;

	return 0;
}

/* IPv4-only version of inet_pton */
static int pton(const char *string, in_addr_t *addr)
{
	grub_uint32_t o1, o2, o3, o4;
	const char *q;
	char *p;

	/* GRUB doesn't have anything useful like scanf(),
	 * so strtoul will have to work
	 */

	q = string;
	o1 = grub_strtoul(q, &p, 10);
	if (q == p || *p != '.') {
		return -1;
	}

	q = p + 1;
	o2 = grub_strtoul(q, &p, 10);
	if (q == p || *p != '.') {
		return -1;
	}

	q = p + 1;
	o3 = grub_strtoul(q, &p, 10);
	if (q == p || *p != '.') {
		return -1;
	}

	q = p + 1;
	o4 = grub_strtoul(q, &p, 10);
	if (q == p || *p != '\0') {
		return -1;
	}

	/* make sure no bits > 7 are set */
	if ((o1 > 255) || (o2 > 255) ||
	    (o3 > 255) || (o4 > 255)) {
		return -1;
	}

	*addr = htonl(o1 << 24 | o2 << 16 | o3 << 8 | o4);

	return 0;
}

/* IPv4-only version of inet_ntop */
static const char *ntop(const in_addr_t *src, char *dest)
{
	in_addr_t addr = ntohl(*src);

	grub_sprintf(dest, "%d.%d.%d.%d",
		       addr >> 24 & 0xff,
		       addr >> 16 & 0xff,
		       addr >>  8 & 0xff,
		       addr >>  0 & 0xff);

	return dest;
}

static int decode_emulab_vendor_options(void *vendor_options, grub_size_t length, in_addr_t *ip)
{
	grub_uint8_t *p = vendor_options;
	int rc = 0;

	while (p < (grub_uint8_t *)vendor_options + length) {
		grub_uint8_t code, len = 0;

		code = *p;

		if (code == DHCP_OPTION_PAD) {
			p++;
		} else if (code == DHCP_OPTION_END) {
			break;
		} else {
			len = *(p + 1);
			p += 2;
			if (code == DHCP_OPTION_EMULAB_BOOTINFO) {
				if (len >= 4)
					*ip = *(in_addr_t *)p;
			}

			p += len;
		}
	}

	return rc;
}

static in_addr_t find_bootinfo_ip(void *dhcpdata, grub_size_t dhcplen)
{
	in_addr_t server_id = 0;
	int option_overload = 0;
	grub_uint8_t type, length;
	char *vendor_options = NULL, *vendor_class_id = NULL;
	int vendor_options_length = 0, vendor_class_id_len = 0;

	server_id = (in_addr_t)grub_pxe_server_ip;

	/* Start looking through the DHCP options
	 * for the Server Identifier option.  Options
	 * start at offset 240 (after the DHCP magic
	 * cookie).
	 */

	grub_printf("Checking DHCP data for bootinfo server address...");
	char *p = (char *)dhcpdata + DHCP_OPTIONS_OFFSET + 4; /* 4 for the cookie */
	do {
		if (p > (char *)dhcpdata + dhcplen) {
			break;
		}

		if (option_overload & 2) {
			/* 'file' field used for options */
			p = (char *)dhcpdata + DHCP_FILE_OFFSET;
			option_overload &= 1;
		}
		else if (option_overload & 1) {
			/* 'sname' field used for options */
			p = (char *)dhcpdata + DHCP_SNAME_OFFSET;
			option_overload = 0;
		}
		while (*(grub_uint8_t *)p != DHCP_OPTION_END) {
			type = *(grub_uint8_t *)p;

			if (type == DHCP_OPTION_PAD)
				length = 1;
			else
				length = *(grub_uint8_t *)(p + 1);

			if (type == DHCP_OPTION_OVERLOAD && length == 1) {
				option_overload = *(grub_uint8_t *)(p + 2);
			} else if (type == DHCP_OPTION_VENDOR && length >= 1) {
				if (!vendor_options) {
					vendor_options = p + 2;
					vendor_options_length = length;
				}
			} else if (type == DHCP_OPTION_VCI && length >= 1) {
				if (!vendor_class_id) {
					vendor_class_id = p + 2;
					vendor_class_id_len = length;
				}
			}

			p += length + 2;
		}
	} while (option_overload);

	if (vendor_class_id && (grub_strncmp(vendor_class_id, EMULAB_VCI,
	                                    vendor_class_id_len) == 0 ||
	                        grub_strncmp(vendor_class_id, "PXEClient",
	                                     9) == 0)) {
		if (vendor_options) {
			decode_emulab_vendor_options(vendor_options,
			                             vendor_options_length,
			                             &server_id);
		}
	}

	return server_id;
}

static int send_request(in_addr_t bootinfo_ip, boot_info_t *boot_info)
{
	int rc = udp_write(bootinfo_ip, htons(BOOTWHAT_DSTPORT),
	               htons(BOOTWHAT_SRCPORT), 0, boot_info,
	               sizeof(boot_info_t));
	if (rc != GRUB_PXENV_STATUS_SUCCESS) {
		grub_printf("udp_write() returned %d\n", rc);
	}

	return rc;
}

/* Try to get a bootinfo response
 *
 * my_ip: destination ip to accept packets for
 * buffer: where to store the response
 * legnth: size of buffer
 * timeout: the time to wait for a response (in clock ticks), or 0 for polling mode
 *
 * This tries to approximate the behavior of having a socket with a receive timeout
 * by polling for approximately timeout clock ticks.
 *
 * returns PXENV_STATUS_SUCCESS if a packet was read.  'length' will now contain
 * the size of the packet.
 *
 * returns PXENV_STATUS_FAILURE if no packet was read or an unknown error occurs.
 */
static int get_response(in_addr_t my_ip, void *buffer, grub_size_t *length, grub_uint32_t timeout)
{
	int rc;
	int key;
	grub_uint64_t start;

	start = grub_get_time_ms();
	do {
		/* We have to handle ESC events here too. Yuck. */
		key = get_key(1);
		if (GRUB_TERM_ASCII_CHAR(key) == GRUB_TERM_ESC) {
			rc = USER_ABORT;
			break;
		}
		rc = udp_read(my_ip, htons(BOOTWHAT_SRCPORT),
			      buffer, (grub_uint16_t *)length);
		if (rc == GRUB_PXENV_STATUS_SUCCESS) {
			/* print_packet(buffer, *length); */
			break;
		}
		else if (rc == -1) {
			rc = GRUB_PXENV_STATUS_FAILURE;
		}
		else if (rc != GRUB_PXENV_STATUS_FAILURE) {
			grub_printf("WARNING: udp_read() returned %d\n", rc);
			break;
		}
		grub_millisleep(1);
	} while (!timeout || ((grub_get_time_ms() - start) < timeout));

	return rc;
}

static int
parse_linux_commandline(char *commandline)
{
	int rc;
	char *p, *token;
	char *buffer = NULL;

	buffer = grub_malloc(grub_strlen(commandline) + 6 + 1);
	if (buffer == NULL) {
		rc = grub_errno;
		goto out;
	}

	grub_strcpy(buffer, commandline);
	p = grub_strchr(buffer, ' ');
	if (p) {
		*p++ = '\0';
	}
	if (*buffer) {
		grub_env_set("bootinfo.kernel", buffer);
	}

	if (p) {
		grub_env_set("bootinfo.kernel_args", p);

		token = grub_strstr(p, "initrd=");
		if (token) {
			p = grub_strchr(token, ' ');
			if (p) {
				*p = '\0';
			}
			grub_env_set("bootinfo.initrd", token + 7);
		}
	}

out:
	if (buffer)
		grub_free(buffer);

	return rc;
}

/* Set up environment variables so that we can pass them
 * to our modified FreeBSD loader or to a kernel that we
 * boot directly.
 *
 * Until we can load FreeBSD kernel modules, we boot a
 * FreeBSD system by loading our modified /boot/loader
 * that will load any environment passed in after parsing
 * loader.conf.  This lets us override image defaults with
 * the bootinfo commandline.
 */
static int
parse_freebsd_commandline(char *commandline)
{
	char *p, *token;
	char *value;
	char *path = NULL;
	char *kernel;
	char *buffer = NULL;
	int rc = 0;

	buffer = grub_malloc(grub_strlen(commandline) + 1);
	if (buffer == NULL) {
		rc = grub_errno;
		goto out;
	}
	grub_strcpy(buffer, commandline);

	path = buffer;
	p = buffer;
	while ((token = grub_strchr(p, ' '))) {
		*token = '\0';
		token++;
		p = token;

		if (*token == ' ') {
			continue;
		}

		p = grub_strchr(p, ' ');
		if (p) {
			*p = '\0';
		}

		value = grub_strchr(token, '=');
		if (value) {
			char name[(value - token) + 8 + 1];
			*value++ = '\0';
			grub_sprintf(name, "kFreeBSD.%s", token);
			grub_env_set(name, value);
		}

		if (p) {
			*p++ = ' ';
		}
	}

	if (path[0] != '\0') {
		grub_env_set("bootinfo.kernel", path);

		/*
		 * For FBSD 5 and above, strip off leading "/boot/" as it will
		 * be assumed and will also screw up the setting of module_path
		 */
		if (grub_strncmp(path, "/boot/", 6) == 0) {
			path += 6;
		}

		/* Tell loader which kernel to boot and where to find it
		 * 	kernel		kernel directory from which to boot
		 * 	bootfile	kernel file to boot
		 */
		if (path[0] != '/' && (kernel = grub_strrchr(path, '/')) != 0) {
			*kernel++ = '\0';
			grub_env_set("kFreeBSD.bootfile", kernel);
			grub_env_set("kFreeBSD.kernel", path);
		} else {
			grub_env_set("kFreeBSD.bootfile", path);

			/*
			 * Such a hack: if the path started with slash (i.e., did not
			 * start with "/boot/"), then it is probably a 4.x boot where
			 * we need to set 'kernel' to the file to boot.
			 */
			if (path[0] == '/') {
				grub_env_set("kFreeBSD.kernel", path);
			}
		}
	}

out:
	if (buffer)
		grub_free(buffer);

	return rc;
}

static grub_err_t
grub_cmd_bootinfo (grub_extcmd_t cmd, int argc __attribute__ ((unused)),
                   char **args __attribute__ ((unused)))
{
	struct grub_arg_list *state = cmd->state;
	boot_info_t *boot_info;
	boot_what_t *boot_what;
	grub_uint8_t buffer[sizeof(boot_info_t) + MAX_BOOT_CMDLINE];
	grub_size_t length;
	in_addr_t my_ip;
	in_addr_t bootinfo_ip;
	char *dhcpdata = NULL;
	grub_size_t dhcplen;
	int rc = 0;
	char *bootinfo_name = NULL;
	char buf[256];
	int key;
	char *p;

	if (! grub_pxe_pxenv) {
		grub_error (GRUB_ERR_FILE_NOT_FOUND, "no pxe environment");
		rc = grub_errno;
		goto out;
	}

	bootinfo_ip = 0;
	if (state[0].set) {
		bootinfo_name = state[0].arg;
	       	if (pton(state[0].arg, &bootinfo_ip) < 0) {
			grub_error (GRUB_ERR_BAD_ARGUMENT, "invalid IP address");
			rc = grub_errno;
			goto out;
		}
	}

	boot_info = (boot_info_t *)buffer;

	/* Extract our IP address from the cached DHCP data */
	if (pxe_get_cached_info(GRUB_PXENV_PACKET_TYPE_DHCP_ACK, &dhcpdata,
	                        &dhcplen) != GRUB_PXENV_STATUS_SUCCESS) {
		grub_error(GRUB_ERR_IO, "failed to get cached IP info");
		rc = grub_errno;
		goto out;
	}

	my_ip = *(in_addr_t *)((char *)dhcpdata + 16);
	if (bootinfo_ip == 0) {
		bootinfo_ip = find_bootinfo_ip(dhcpdata, dhcplen);
		if (bootinfo_ip == 0) {
			grub_printf("Unable to find bootinfo address\n");
			grub_free(dhcpdata);
			return -1;
			/* FIXME */
		}

		ntop(&bootinfo_ip, buf);
		bootinfo_name = buf;
	}

	grub_free(dhcpdata);

	udp_close();
	rc = udp_open(my_ip);
	if (rc != GRUB_PXENV_STATUS_SUCCESS) {
		grub_error(GRUB_ERR_IO, "udp_open_failed");
		rc = grub_errno;
		goto out;
	}

	grub_printf("Requesting bootinfo data from %s...", bootinfo_name);
	while(1) {
		grub_memset(buffer, 0, sizeof(buffer));
		boot_info->version = BIVERSION_CURRENT;
		boot_info->opcode  = BIOPCODE_BOOTWHAT_REQUEST;

		rc = send_request(bootinfo_ip, boot_info);
		if (rc != GRUB_PXENV_STATUS_SUCCESS && rc != GRUB_PXENV_STATUS_FAILURE) {
			grub_error(GRUB_ERR_IO, "failed to send bootinfo request");
			rc = grub_errno;
			goto out;
		}

		grub_printf(".");

		boot_what = (boot_what_t *)boot_info->data;
		boot_what->type = 0;

		length = sizeof(buffer);

		/* Wait up to 10 seconds(-ish) for a response */
		rc = get_response(my_ip, boot_info, &length, 10 * 1000);
		if (rc != GRUB_PXENV_STATUS_SUCCESS && rc != GRUB_PXENV_STATUS_FAILURE) {
			grub_printf("\nerror when waiting for bootinfo response: %d\n",
					rc);
			grub_error(GRUB_ERR_IO, "failed to receive bootinfo response");
			rc = grub_errno;
			goto out;
		}

		if (rc == GRUB_PXENV_STATUS_SUCCESS)
			grub_printf(" ok\n");

		while (boot_what->type == BIBOOTWHAT_TYPE_WAIT) {
			grub_printf("Polling for bootinfo response... ");
			length = sizeof(buffer);
			rc = get_response(my_ip, boot_info, &length, 0);
			if (rc != GRUB_PXENV_STATUS_SUCCESS) {
				grub_printf("\nerror when waiting for bootinfo response: %d\n",
						rc);
				grub_error(GRUB_ERR_IO, "failed to receive bootinfo response");
				rc = grub_errno;
				goto out;
			}
			grub_printf(" ok\n");
		}

		switch (boot_what->type) {
			case BIBOOTWHAT_TYPE_REBOOT:
				grub_printf("Rebooting...\n");
				return grub_command_execute("reboot", 0, NULL);
				break;
			case BIBOOTWHAT_TYPE_DISKPART:
				if (boot_what->what.dp.partition > 0) {
					grub_sprintf(buf, "hd%d,%d", boot_what->what.dp.disk - 0x80,
						     boot_what->what.dp.partition);
				} else {
					grub_sprintf(buf, "hd%d", boot_what->what.dp.disk - 0x80);
				}
				grub_env_set("bootinfo.orig_root", buf);

				if (boot_what->cmdline[0] != '\0') {
					grub_env_set("bootinfo.orig_cmdline", boot_what->cmdline);
				}
				rc = GRUB_ERR_NONE;
				goto out;
				break;
			case BIBOOTWHAT_TYPE_PART:
				if (boot_what->what.partition > 0) {
					grub_sprintf(buf, "hd%d,%d", 0, boot_what->what.partition);
				} else {
					grub_sprintf(buf, "hd%d", 0);
				}
				grub_env_set("bootinfo.orig_root", buf);

				if (boot_what->cmdline[0] != '\0') {
					grub_env_set("bootinfo.orig_cmdline", boot_what->cmdline);
				}
				rc = GRUB_ERR_NONE;
				goto out;
				break;
			case BIBOOTWHAT_TYPE_MFS:
				grub_env_set("bootinfo.orig_root", "pxe");
				if (boot_what->cmdline[0] != '\0') {
					grub_env_set("bootinfo.orig_cmdline", boot_what->cmdline);
				}

				p = grub_strchr(boot_what->what.mfs, ':');
				if (p) {
					*p = '\0';
					grub_env_set("bootinfo.netboot_server", boot_what->what.mfs);
					grub_env_set("bootinfo.netboot_path", p + 1);
					*p = ':';
				} else {
					grub_env_set("bootinfo.netboot_path", boot_what->what.mfs);
				}

				rc = GRUB_ERR_NONE;
				goto out;
				break;
			case BIBOOTWHAT_TYPE_AUTO:
				grub_printf("query: will query again\n");
				continue;
				break;
			case BIBOOTWHAT_TYPE_SYSID:
				grub_printf("request to boot partition type 0x%02x not supported\n",
						boot_what->what.sysid);
				break;
		}

		/* Only sleep if we got a bogus response or we are to
		 * go through the loop again.
		 *
		 * Use get_key() to do the sleep so we can catch ESC
		 * keypresses.
		 */
		if (rc != GRUB_PXENV_STATUS_FAILURE) {
			grub_printf("Unexpected boot type %d\n", boot_what->type);
			grub_printf("Retrying...");
			key = get_key(5 * 1000);
			if (GRUB_TERM_ASCII_CHAR(key) == GRUB_TERM_ESC) {
				grub_printf("\n");
				rc = GRUB_ERR_NONE;
				break;
			}
		}

	}

out:
	udp_close();

	return rc;
}

static grub_err_t
grub_cmd_bootinfo_parse_cmdline (grub_extcmd_t cmd, int argc, char **args)
{
	int rc = GRUB_ERR_NONE;
	struct grub_arg_list *state = cmd->state;

	if (argc == 0)
		return grub_error (GRUB_ERR_INVALID_COMMAND, "no argument specified");

	if (state[0].set) {
		rc = parse_linux_commandline(args[0]);
	} else if (state[1].set) {
		rc = parse_freebsd_commandline(args[0]);
	}

	return rc;
}

#if 0
static void
print_ip (grub_uint32_t ip)
{
  int i;

  for (i = 0; i < 3; i++)
    {
      grub_printf ("%d.", ip & 0xFF);
      ip >>= 8;
    }
  grub_printf ("%d", ip);
}
#endif

static const struct grub_arg_option bootinfo_options[] =
{
	{"server", 's', 0, "IP address of bootinfo server", "SERVER", ARG_TYPE_STRING},
	{0, 0, 0, 0, 0, 0}
};

static const struct grub_arg_option parse_cmdline_options[] =
{
	{"linux", 'l', 0, "parse arguments as Linux command line", 0, 0},
	{"freebsd", 'f', 0, "parse arguments as FreeBSD command line", 0, 0},
	{0, 0, 0, 0, 0, 0}
};

static grub_extcmd_t ecmd_1, ecmd_2;
GRUB_MOD_INIT(bootinfo)
{
  (void) mod;			/* To stop warning. */
  ecmd_1 = grub_register_extcmd ("bootinfo", grub_cmd_bootinfo, GRUB_COMMAND_FLAG_BOTH,
		  "bootinfo [-s SERVER]",
                  "Command to obtain boot spec from bootinfo server.", bootinfo_options);

  ecmd_2 = grub_register_extcmd ("bootinfo_parse_cmdline", grub_cmd_bootinfo_parse_cmdline, GRUB_COMMAND_FLAG_BOTH,
		  "bootinfo_parse_cmdline [-l|-f] COMMANDLINE",
		  "Set the environment variables bootinfo.kernel, bootinfo.kernel_args,"
		  " and bootinfo.initrd (Linux only) using COMMANDLINE.",
		  parse_cmdline_options);
}

GRUB_MOD_FINI(bootinfo)
{
  grub_unregister_extcmd (ecmd_1);
  grub_unregister_extcmd (ecmd_2);
}
