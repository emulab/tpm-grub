/* pxe.c - command to control the pxe driver  */
/*
 *  GRUB  --  GRand Unified Bootloader
 *  Copyright (C) 2008  Free Software Foundation, Inc.
 *
 *  GRUB is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  GRUB is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with GRUB.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <grub/dl.h>
#include <grub/err.h>
#include <grub/misc.h>
#include <grub/machine/pxe.h>
#include <grub/extcmd.h>

#define htonl(x)	(grub_cpu_to_be32(x))

static const struct grub_arg_option options[] =
{
    {"info", 'i', 0, "show PXE information.", 0, 0},
    {"bsize", 'b', 0, "set PXE block size", 0, ARG_TYPE_INT},
    {"unload", 'u', 0, "unload PXE stack.", 0, 0},
    {"server", 's', 0, "set TFTP server address", "SERVER", ARG_TYPE_STRING},
    {0, 0, 0, 0, 0, 0}
  };

static void
print_ip (grub_uint32_t ip)
{
  int i;

  for (i = 0; i < 3; i++)
    {
      grub_printf ("%d.", ip & 0xFF);
      ip >>= 8;
    }
  grub_printf ("%d", ip);
}

/* IPv4-only version of inet_pton */
static int pton(const char *string, grub_uint32_t *addr)
{
	grub_uint32_t o1, o2, o3, o4;
	const char *q;
	char *p;

	/* GRUB doesn't have anything useful like scanf(),
	 * so strtoul will have to work
	 */

	q = string;
	o1 = grub_strtoul(q, &p, 10);
	if (q == p || *p != '.') {
		return -1;
	}

	q = p + 1;
	o2 = grub_strtoul(q, &p, 10);
	if (q == p || *p != '.') {
		return -1;
	}

	q = p + 1;
	o3 = grub_strtoul(q, &p, 10);
	if (q == p || *p != '.') {
		return -1;
	}

	q = p + 1;
	o4 = grub_strtoul(q, &p, 10);
	if (q == p || *p != '\0') {
		return -1;
	}

	/* make sure no bits > 7 are set */
	if ((o1 > 255) || (o2 > 255) ||
	    (o3 > 255) || (o4 > 255)) {
		return -1;
	}

	*addr = htonl(o1 << 24 | o2 << 16 | o3 << 8 | o4);

	return 0;
}

static grub_err_t
grub_cmd_pxe (grub_extcmd_t cmd, int argc __attribute__ ((unused)),
	      char **args __attribute__ ((unused)))
{
  struct grub_arg_list *state = cmd->state;

  if (! grub_pxe_pxenv)
    return grub_error (GRUB_ERR_FILE_NOT_FOUND, "no pxe environment");

  if (state[3].set) {
	  if (pton(state[3].arg, &grub_pxe_server_ip) < 0) {
		  return grub_error (GRUB_ERR_BAD_ARGUMENT, "invalid IP address");
	  }

	  return GRUB_ERR_NONE;
  }

  if (state[1].set)
    {
      int size;

      size = grub_strtoul (state[1].arg, 0, 0);
      if (size < GRUB_PXE_MIN_BLKSIZE)
        size = GRUB_PXE_MIN_BLKSIZE;
      else if (size > GRUB_PXE_MAX_BLKSIZE)
        size = GRUB_PXE_MAX_BLKSIZE;

      grub_pxe_blksize = size;
    }

  if (state[0].set)
    {
      grub_printf ("blksize : %d\n", grub_pxe_blksize);
      grub_printf ("client ip  : ");
      print_ip (grub_pxe_your_ip);
      grub_printf ("\nserver ip  : ");
      print_ip (grub_pxe_server_ip);
      grub_printf ("\ngateway ip : ");
      print_ip (grub_pxe_gateway_ip);
      grub_printf ("\n");
    }

  if (state[2].set)
    grub_pxe_unload ();

  return 0;
}

static grub_extcmd_t cmd;

GRUB_MOD_INIT(pxecmd)
{
  cmd = grub_register_extcmd ("pxe", grub_cmd_pxe, GRUB_COMMAND_FLAG_BOTH,
			      "pxe [-i|-b|-s|-u]",
			      "Command to control the PXE device.", options);
}

GRUB_MOD_FINI(pxecmd)
{
  grub_unregister_extcmd (cmd);
}
