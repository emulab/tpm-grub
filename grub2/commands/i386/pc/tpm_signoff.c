/*
 *  GRUB  --  GRand Unified Bootloader
 *  Copyright (C) 2008  Free Software Foundation, Inc.
 *
 *  GRUB is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  GRUB is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with GRUB.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <grub/normal.h>
#include <grub/dl.h>
#include <grub/lib/arg.h>
#include <grub/err.h>
#include <grub/misc.h>
#include <grub/time.h>
#include <grub/term.h>
#include <grub/machine/pxe.h>
#include <grub/machine/memory.h>
#include <grub/mm.h>
#include <grub/device.h>
#include <grub/disk.h>
#include <grub/fs.h>
#include <grub/file.h>
#include <grub/extcmd.h>

static grub_err_t
grub_cmd_tpm_signoff(grub_extcmd_t UNUSED cmd, int argc , char **args)
{
	int pcr;

	if (argc != 1) {
		grub_printf("Incorrect syntax.\n");
		return 1;
	}

	pcr = grub_strtoul(args[0], NULL, 10);
	grub_printf("Extending crap into PCR %d\n", pcr);

	if (pcr < 0 || pcr > 23) {
		grub_printf("PCR index out of range.\n");
		return 1;
	}

	 if (tpm_extend(pcr))
		grub_printf("Error extending\n");

	grub_printf("Done\n");

	return 0;
}

static grub_extcmd_t ecmd_1;
GRUB_MOD_INIT(tpm_signoff)
{
  (void) mod;			/* To stop warning. */
  ecmd_1 = grub_register_extcmd("tpm_signoff", grub_cmd_tpm_signoff,
	GRUB_COMMAND_FLAG_BOTH, "tpm_singoff [PCR]",
	"Extends rubbish into the given PCR.", NULL);
}

GRUB_MOD_FINI(tpm_signoff)
{
  grub_unregister_extcmd(ecmd_1);
}
